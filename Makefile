CC=gcc
CFLAGS=-g -Wall
IFLAGS=-Iinclude
LFLAGS=-lncurses
SRC=src
BIN=bin
BINNAME=soko

all:
	$(CC) $(CFLAGS) $(IFLAGS) $(SRC)/*.c -o $(BIN)/$(BINNAME) $(LFLAGS)
