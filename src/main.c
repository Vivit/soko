#include <curses.h>
#include <error.h>

#include "display.h"
#include "input.h"
#include "level.h"

void cmd_loop();
enum dir chardir(char c);

int main(int argc, char **argv) {
    alloc_board();
    if (argc == 1) {
        error(1, 0, "Usage: %s FILE", argv[0]);
    }
    read_file(argv[1]);

    init_display();
    printboard();

    cmd_loop();

    free_board();
    close_display();
}

void cmd_loop() {
    char cmd;
    do {
        cmd = getch();
        makemove(chardir(cmd));
    } while (cmd != 'q');
}
