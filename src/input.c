#include <curses.h>
#include <error.h>

#include "input.h"
#include "level.h"

enum dir chardir(char c) {
    switch (c) {
        case 'h':   return DIR_W;
        case 'j':   return DIR_S;
        case 'k':   return DIR_N;
        case 'l':   return DIR_E;
        case 'y':   return DIR_NW;
        case 'u':   return DIR_NE;
        case 'b':   return DIR_SW;
        case 'n':   return DIR_SE;
        default:    return DIR_NONE;
    }

    /* Can't happen */
    return -1;
}

enum dir getdir() { return chardir(getch()); }
