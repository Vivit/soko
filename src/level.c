#include <stdlib.h>
#include <error.h>
#include <string.h>

#include "input.h"

#include "display.h"
#include "err.h"
#include "level.h"



      /*** ALLOCATING MEMORY AND READING LEVELS FROM RAW TEXT FILES ***/
/* The filename of the file we're reading */
char *filename;

/* Global board.  Not REALLY sure while I'm dynamically allocating this; FIXME. */
struct cell **board;

/* Sanity checks for levels read from files.
 * To be valid, a level must have the same number of pallets as crates, must
 * be within the bounds of WIDTH */
void err_doppelganger(int line);
void err_invent(int gl, int line);

/* The following two functions allocate and the data for the global board.  The
 * board will be reused throughout execution of the program, so this function
 * should only be called once. */
void alloc_board() {
    board = malloc(HEIGHT * sizeof (enum tile*));
    for (int i = 0; i < HEIGHT; i++) {
        board[i] = malloc(WIDTH * sizeof (struct cell));
    }

    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++) {
            board[i][j] = CELL_UNDEF;
        }
    }
}

void free_board() {
    for (int i = 0; i < HEIGHT; i++) {
        free(board[i]);
    }
    free(board);
}

/* Reads file from disk. */
void read_file(const char *fn) {
    filename = realloc(filename, (1 + strlen(fn)) * sizeof (char));
    strcpy(filename, fn);
    FILE *f = fopen(fn, "r");

    if ((f) == NULL) error(1, 0, "cannot access '%s': No such file or "
            "directory",
            fn);

    init_board(f);

    free(filename);
    filename = NULL;
}

#if 0
/* Makes a struct cell out of a tiletype and a pair of coordinates. */
struct cell mkcell(struct coords c, enum tile tiletype) {
    
}
#endif

/* Constructs a board from text data in a file and saves it to global board.
 *
 * This function is a bit confusing, because we have two indices to keep track
 * of -- and the latter one is actually a pair of coordinates.
 *
 * The first is the file pointer, which the stdio library takes care of, 
 * marking the next character to be read from *in by fgetc().  The pair of
 * coordinates are the.
 *
 * I use some of the homelier flow control constructs in the C language to
 * abstract some of the procedures involving the  */
void init_board(FILE *f) {
    /* col and row respectively record the row and column of the cursor on the
     * over the board -- NOT to be confused with the file pointer in *f,
     * which advances on every call to fgetc(). */
    int row = 0, col = 0;

    /* Sanity check variables: */ 
    int surplus = 0;    /* This should be 0 */
    int you_exist = 0;  /* This should be 1 */

    enum tile thing_here = 0;

    char c = fgetc(f);
    
    while (c != EOF) {
        /* If we're over the edge, advance the file pointer to the next '\n',
         * then do a carriage return. */
        if (col >= WIDTH) goto endl;

        /* We've reached the end of the map. */
        if (row >= HEIGHT) break;

        switch (c) {
            /* On newline character, goto carriage return. */
            case '\n':
                goto cr;

            /* The following cases all set board[row][col] to c, hence each one
             * ending in . */
            case (char) TILE_PALLET:
                surplus--;
                goto set;

            case (char) TILE_CRATE:
                thing_here = TILE_CRATE;
                surplus++;
                goto set;

            case (char) TILE_YOU:
                /* Sanity check: are there two of the player? */
                if (you_exist) err_doppelganger(row);
                you_exist = 1;
                thing_here = TILE_YOU;
                you = (struct coords) { .row = row, .col = col };
                /* fall through */

            case (char) TILE_WALL:
            case (char) TILE_FLOOR:
set:
                board[row][col] = (struct cell) { .tiletype = c,
                                    .thinghere = thing_here,
                                    .loc = { .row = row, .col = col }
                };
                col++;
                break;

            default:
                board[row][col] = CELL_UNDEF;
                col++;
                break;

/** The following labels move cursor and/or the file pointer around **/
/* Move forward until an EOL or EOF is found. */
endl:
            while (c != EOF && c != '\n') { c = fgetc(f); }

/* "Carriage return" -- return to the left side and going down. */
cr:
            col = 0;
            row++;
            break;
        }
        thing_here = 0;
        c = fgetc(f);
    }

    /* At this point we don't need f anymore, and weird things seem to happen
     * to it beyond this point, so maybe this should duct-tape the leak shut.
     * FIXME. */
    fclose(f);

    /* Sanity check: do we have more crates than pallets or more
     * pallets than crates? */
    if (surplus != 0) err_invent(surplus > 0, row);

    /* Fill in the rest of the grid with TILE_UNDEF */
    for (; row < HEIGHT; row++) {
        for (; col < WIDTH; col++) {
            board[row][col] = CELL_UNDEF;
        }
        col = 0;
    }
}


/** Sanity checks -- all of these terminate execution **/
/* Raised when two '@'s are read from a level map. */
void err_doppelganger(int line) {
    crash_gracefully();
    error_at_line(1, 0, filename, line,
            "Two starting positions on the same level!");
}

/* Raised when there are more crates than there are pallets or vice-versa. */
void err_invent(int gl, int line) {
    crash_gracefully();
    error(2, 0, "Level in %s has more %s.", filename,
            gl ? "crates than pallets" : "pallets than crates");
}



              /*** COORDINATES, TILE ADDRESSING, AND MOVEMENT ***/
struct coords you;
struct coords adjacent(struct coords,enum dir);

/** Coordinates and Tile Addressing **/

/* Return the immediately adjacent coordinates to coordinate pair c and in
 * direction d.  We'll only use this in this file, so we're forward-declaring
 * it here. */
struct coords adjacent(struct coords c, enum dir d) {
    switch (d) {
        case DIR_W: return (struct coords) {.row = c.row, .col = c.col-1};
        case DIR_S: return (struct coords) {.row = c.row+1, .col = c.col};
        case DIR_N: return (struct coords) {.row = c.row-1, .col = c.col};
        case DIR_E: return (struct coords) {.row = c.row, .col = c.col+1};
        case DIR_NW: return (struct coords) {.row = c.row-1, .col = c.col-1};
        case DIR_NE: return (struct coords) {.row = c.row-1, .col = c.col+1};
        case DIR_SW: return (struct coords) {.row = c.row+1, .col = c.col-1};
        case DIR_SE: return (struct coords) {.row = c.row+1, .col = c.col+1};
        case DIR_NONE: return c;
        default: break;
    }

    /* Can't happen */
    crash_gracefully();
    error(1, 0, "Unknown direction %d passed to adjacent().", d);

    /* CANNOT happen */
    return you;
}

/* Return the cell at the given coordinates */
struct cell *cellat(struct coords c) { return &board[c.row][c.col]; }

/* Return the type of the tile at the given coordinates. */
enum tile tileat(struct coords c) { return cellat(c)->tiletype; }

/* Return the type of thing at the given coordinates, if anything. */
enum tile thingat(struct coords c) { return cellat(c)->thinghere; }


/** Movement of Pieces **/

/* Attempt to move one square.  If blocked by a wall, stop.  If blocked by a
 * crate, attempt to push that crate. */
int makemove(enum dir d) {
    struct coords dest = adjacent(you, d);

    /* Handle diagonal movement.  The player should not be able to move between
     * two solid objects diagonally; diagonal movement is only a nicety due to
     * NetHack.  You may, however, move diagonally around open corners. You just
     * can't use diagonal movement to reach places you can't also reach via
     * orthogonal movement alone. */
    switch (d) {
        case DIR_NW:
            if (passible(adjacent(you, DIR_N)) ||
                    passible(adjacent(you, DIR_W)))
                break;
            else return 0;

        case DIR_NE:
            if (passible(adjacent(you, DIR_N)) ||
                    passible(adjacent(you, DIR_E)))
                break;
            else return 0;

        case DIR_SW:
            if (passible(adjacent(you, DIR_S)) ||
                    passible(adjacent(you, DIR_W)))
                break;
            else return 0;

        case DIR_SE:
            if (passible(adjacent(you, DIR_S)) ||
                    passible(adjacent(you, DIR_E)))
                break;
            else return 0;

        default:
            break;
    }

    if (cellat(dest)->thinghere == TILE_CRATE && !pushcrate(dest, d))
        return 0;

    if (tileat(dest) == TILE_WALL)
        /* No space to move forward. Do nothing. */
        return 0;

    if (tileat(dest) == TILE_UNDEF) {
        crash_gracefully();
        error(1, 0, "You makemove()'d into nowhere!");
    }

    cellat(you)->thinghere = 0;
    updtile(you);
    you = adjacent(you, d);
    cellat(you)->thinghere = TILE_YOU;
    updtile(you);
    return 1;
}

/* Can this tile be passed through without pushing anything? */
int passible(struct coords c) {
    return tileat(c) != TILE_WALL && tileat(c) != TILE_UNDEF
        && !cellat(c)->thinghere;
}

/* Try to push crate at coords c one square in direction d. */
int pushcrate(struct coords c, enum dir d) {
    /* Diagonal pushing not allowed! */
    if (d == DIR_NW || d == DIR_NE || d == DIR_SW || d == DIR_SE) {
        return 0;
    }

    /* Cell containing crate to be pushed */
    struct cell *orig = cellat(c);

    /* Cell into which we intend to push the crate */
    struct cell *dest = cellat(adjacent(c, d));

    if (!orig->thinghere) {
        crash_gracefully();
        error(1, 0, "Attempted to push a crate at row %d, column %d, but %s",
                c.row, c.col, "no crate was found!");
    }

    if (orig->thinghere == TILE_YOU) {
        crash_gracefully();
        error(1, 0, "Take it easy; don't push yourself!");
    }

    /* Fail if no space for crate. */
    if (dest->thinghere     == TILE_CRATE ||
            dest->thinghere == TILE_YOU   ||
            dest->tiletype  == TILE_WALL  ||
            dest->tiletype  == TILE_UNDEF)
        return 0;

    dest->thinghere = orig->thinghere;
    orig->thinghere = 0;
    updtile(dest->loc);
    updtile(orig->loc);
    return 1;
}
