#include <stdio.h>

#include "level.h"
#include "display.h"

void crash_gracefully() {
    close_display();
    free_board();
}
