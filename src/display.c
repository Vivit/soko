#include <curses.h>

#include "level.h"
#include "display.h"

void init_display() {
    initscr();
    noecho();
}

void close_display() {
    echo();
    endwin();
}

void printboard() {
    struct coords ctmp;
    for (ctmp.row = 0; ctmp.row < HEIGHT; ctmp.row++) {
        for (ctmp.col = 0; ctmp.col < WIDTH; ctmp.col++) {
            updtile(ctmp);
        }
    }
}

void updtile(struct coords c) {
    char ch = '\0'; /* Can't happen */

    if (thingat(c))
        ch = thingat(c);
    else switch (tileat(c)) {
        case TILE_WALL: 
        case TILE_PALLET:
        case TILE_FLOOR:
        case TILE_UNDEF:
            ch = tileat(c);
            break;

        default:
            ch = TILE_FLOOR;
    }

    mvaddch(c.row, c.col, ch);
    move(you.row, you.col);
    refresh();
}
