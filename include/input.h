#ifndef __INPUT__
#define __INPUT__

enum dir chardir(char c);
enum dir getdir();

enum dir {
    DIR_NONE,
    DIR_W,
    DIR_S,
    DIR_N,
    DIR_E,
    DIR_NW,
    DIR_NE,
    DIR_SW,
    DIR_SE
};

#endif
