#ifndef __LEVEL__
#define __LEVEL__
#include <stdio.h> /* For the FILE typedef */
#include "input.h" /* For enumations */

#define WIDTH   80
#define HEIGHT  24

/* Row comes becore column because that's the way the curses library handles
 * it.  They're called "row" and "column" instead of "x" and "y" to avoid
 * confusion with standard Cartesian ordered pairs.
 *
 * Also note that, unlike on the standard cartesian plane, increasing your row
 * moves you _down_ the page, from e.g. the 4th row to the 5th row. */
struct coords {
    int row, col;
};

enum tile {
    TILE_UNDEF = ' ',
    TILE_WALL = '%',
    TILE_FLOOR = '.',
    TILE_CRATE = '=',
    TILE_PALLET = '+',
    TILE_CRATE_ON_PALLET = 'O',
    TILE_YOU = '@'
};

#define YOU_HERE    1
#define CRATE_HERE  2

struct cell {
    struct coords loc;
    enum tile tiletype;
    int thinghere;
};

/* A macro for an undefined cell.  Location is set to { -1, -1 } (invalid
 * coordinates the player can't reach) and tiletype and thinghere are both
 * set to zero, meaning "tile type undefined" and "nothing is here",
 * respectively. */
#define CELL_UNDEF (struct cell) { .loc = (struct coords) { -1, -1 }, \
    .tiletype = TILE_UNDEF, \
    .thinghere = 0 \
}

extern struct cell **board;

void read_file(const char*);
void init_board(FILE*);

void alloc_board();
void free_board();

extern struct coords you;

struct cell *cellat(struct coords);
enum tile tileat(struct coords);
enum tile thingat(struct coords c);
int makemove(enum dir);
int passible(struct coords);
int pushcrate(struct coords,enum dir);

#endif
